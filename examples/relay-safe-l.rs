use shadowsocks::*;
use tokio::net::TcpStream;
use tokio::io::{AsyncWriteExt};
use std::sync::Arc;

#[tokio::main]
async fn main() -> Res<()> {
    env_logger::try_init()?;

    // cipher
    let password = "meteorcat123";
    let method = "chacha20_poly1305";
    let cipher = CipherHandler::new(method,password)?;


    // local
    let local_address = "127.0.0.1:7878";
    let address = "127.0.0.1:17878";
    let acceptor = TcpListener::bind(address).await?;
    loop {

        match acceptor.accept().await{
            Ok((inbound,_)) => {
                let outbound_address = local_address.to_string();
                let cipher_handler = cipher.clone();
                tokio::spawn(async move {
                    if let Err(e) = accept(
                        Arc::new(cipher_handler),
                        inbound,
                        outbound_address
                    ).await{
                        error!("{:?}",e);
                    }
                });
            }
            Err(e) => { error!("{:?}",e)}
        }
    }
}


async fn accept(cipher:Arc<CipherHandler>,mut inbound:TcpStream,outbound_address:String)->Res<()>{
    let mut outbound = TcpStream::connect(outbound_address).await?;
    let (mut ri,mut wi) = inbound.split();
    let (mut ro,mut wo) = outbound.split();


    let decrypt_cipher = cipher.clone();
    let encrypt_cipher = cipher.clone();

    tokio::try_join!(
         async move {
            EncryptStream::with_capacity(decrypt_cipher,&mut ri,&mut wo,64 * 1024).await?;
            wo.shutdown().await
         },
         async move {
            DecryptStream::with_capacity(encrypt_cipher,&mut ro,&mut wi,64 * 1024).await?;
            wi.shutdown().await
         }
    )?;

    Ok(())
}
