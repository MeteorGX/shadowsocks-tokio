use shadowsocks::*;
use tokio::net::TcpStream;
use tokio::io::AsyncWriteExt;

#[tokio::main]
async fn main() -> Res<()> {

    env_logger::try_init()?;

    // proxy local ssh
    let ssh_address = "127.0.0.1:22";
    let proxy_address = "127.0.0.1:17878";
    let proxy_server = TcpListener::bind(proxy_address).await?;

    loop{
        match proxy_server.accept().await{
            Err(e) => error!("{:?}",e),
            Ok((inbound,_))=>{
                debug!("Inbound = {:?}",inbound);
                match tokio::time::timeout(
                    tokio::time::Duration::from_secs(10),
                    TcpStream::connect(ssh_address)
                ).await{
                    Err(e) => { error!("{:?}",e) }
                    Ok(res) => match res{
                        Err(e) => { error!("{:?}",e) }
                        Ok(outbound) => {
                            debug!("Outbound = {:?}",outbound);
                            tokio::spawn(async move {
                                match swap(inbound,outbound).await{
                                    Err(_e) => { error!("proxy timeout") },
                                    Ok(_) => {
                                        info!("Leave Session");
                                    },
                                };
                            });
                        }
                    }
                };
            }
        }
    }
}


async fn swap(mut inbound:TcpStream,mut outbound:TcpStream)->Res<((),())>{

    let (mut ri,mut wi) = inbound.split();
    let (mut ro,mut wo) = outbound.split();
    Ok(tokio::try_join!(
        async move {
            Relay::with_capacity(&mut ri,&mut wo,64 * 1024).await?;
            wo.shutdown().await
        },
        async move {
            Relay::with_capacity(&mut ro,&mut wi,64 * 1024).await?;
            wi.shutdown().await
        }
    )?)
}
