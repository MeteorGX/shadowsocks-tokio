use shadowsocks::*;
use tokio::net::TcpStream;
use tokio::io::AsyncWriteExt;

#[tokio::main]
pub async fn main() -> Res<()> {

    env_logger::try_init()?;

    // address
    let socks_address = "127.0.0.1:7979";
    let socks_acceptor = TcpListener::bind(socks_address).await?;

    // listener
    loop{
        let (inbound,_) = match socks_acceptor.accept().await{
            Ok(res) => res,
            Err(e) => {
                error!("{:?}",e);
                continue;
            }
        };
        info!("Inbound = {:?}",inbound);
        if let Err(e) = run(inbound).await{
            error!("{:?}",e);
        }
    }
}


async fn run(mut inbound:TcpStream)->Res<()>{
    let mut socks = SocksBuilder::witch_method(
        SocksVersion::V5,
        SocksMethod::NoAuth
    );


    //+----+----------+----------+
    // |VER | NMETHODS | METHODS  |
    // +----+----------+----------+
    // | 1  |    1     | 1 to 255 |
    // +----+----------+----------+
    let _methods = socks.read_methods(&mut inbound).await?; // read[methods]


    //+----+--------+
    // |VER | METHOD |
    // +----+--------+
    // | 1  |   1    |
    // +----+--------+
    socks.write_method(&mut inbound).await?;// write[method]


    //+----+-----+-------+------+----------+----------+
    // |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
    // +----+-----+-------+------+----------+----------+
    // | 1  |  1  | X'00' |  1   | Variable |    2     |
    // +----+-----+-------+------+----------+----------+
    let address = socks.read_address(&mut inbound).await?;//read[address]
    info!("Socks Info = {:?}",address);




    //+----+-----+-------+------+----------+----------+
    // |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
    // +----+-----+-------+------+----------+----------+
    // | 1  |  1  | X'00' |  1   | Variable |    2     |
    // +----+-----+-------+------+----------+----------+
    let outbound = match address.address{
        SocksAddress::IPv4(address, port)  =>
            TcpStream::connect(format!("{}:{}",address.to_string(),port)).await?,

        SocksAddress::IPv6(address, port) =>
            TcpStream::connect(format!("{}:{}",address.to_string(),port)).await?,

        SocksAddress::Hostname(address, port) =>
            TcpStream::connect(format!("{}:{}",address.to_string(),port)).await?,

        SocksAddress::Unknown => {
            socks.write_response(
                &mut inbound,
                &SocksResponse::HostUnreachable,
                &SocksAddress::Hostname("127.0.0.1".to_string(),7878)
            ).await?;
            return Err(Error::SocksParseFailed);
        }
    };


    info!("Outbound = {:?}",outbound);
    socks.write_response(
        &mut inbound,
        &SocksResponse::Success,
        &SocksAddress::Hostname(outbound.peer_addr()?.to_string(),outbound.peer_addr()?.port())
    ).await?;



    // swap
    tokio::spawn(async move{
        if let Err(e) = swap(inbound,outbound).await{
            error!("{:?}",e);
        };
    });
    Ok(())
}


async fn swap(mut inbound:TcpStream,mut outbound:TcpStream)->Res<()>{
    let (mut ri,mut wi) = inbound.split();
    let (mut ro,mut wo) = outbound.split();
    tokio::try_join!(
        async move {
            Relay::with_capacity(&mut ri,&mut wo,64 * 1024).await?;
            wo.shutdown().await
        },
        async move {
            Relay::with_capacity(&mut ro,&mut wi,64 * 1024).await?;
            wi.shutdown().await
        }
    )?;
    Ok(())
}