use shadowsocks::*;
use tokio::net::TcpStream;


#[tokio::main]
async fn main() -> Res<()> {
    env_logger::try_init()?;

    let proxy_address = "www.meteorcat.com:443";
    let proxy_info : Vec<&str> = proxy_address.split(":").collect();
    let address = proxy_info.get(0).unwrap();
    let port:u16 = proxy_info.get(1).unwrap().parse()?;

    let socks_address = "127.0.0.1:7979";
    let socks_server = TcpStream::connect(socks_address).await?;
    run(socks_server,address.as_ref(),port).await?;

    Ok(())
}


async fn run(mut inbound:TcpStream,address:&str,port:u16)->Res<()>{
    let mut socks = SocksBuilder::witch_method(
        SocksVersion::V5,
        SocksMethod::NoAuth
    );


    //+----+----------+----------+
    // |VER | NMETHODS | METHODS  |
    // +----+----------+----------+
    // | 1  |    1     | 1 to 255 |
    // +----+----------+----------+
    socks.write_methods(&mut inbound).await?; // write[methods]


    //+----+--------+
    // |VER | METHOD |
    // +----+--------+
    // | 1  |   1    |
    // +----+--------+
    let _method = socks.read_method(&mut inbound).await?;// read[method]


    //+----+-----+-------+------+----------+----------+
    // |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
    // +----+-----+-------+------+----------+----------+
    // | 1  |  1  | X'00' |  1   | Variable |    2     |
    // +----+-----+-------+------+----------+----------+
    socks.write_address(&mut inbound,&SocksData{
        cmd: SocksCommand::Connect,
        address: SocksAddress::Hostname(
            address.to_string(),port
        )
    }).await?;//write[address]


    //+----+-----+-------+------+----------+----------+
    // |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
    // +----+-----+-------+------+----------+----------+
    // | 1  |  1  | X'00' |  1   | Variable |    2     |
    // +----+-----+-------+------+----------+----------+
    let (rep,address) = socks.read_response(&mut inbound).await?;
    if let SocksResponse::Success = rep {
        info!("BND = {:?}",address);
    }else{
        return Err(Error::SocksParseFailed);
    }
    Ok(())
}
