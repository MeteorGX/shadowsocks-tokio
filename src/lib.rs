// Copyright 2014-2015 The Rust Project Developers. See the COPYRIGHT
// file at the top-level directory of this distribution and at
// http://rust-lang.org/COPYRIGHT.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.


/// import logging handler
///
///
pub use log::{log,error,warn, info, debug,trace};


/// import custom modules(error)
mod error;
pub use error::{EBox,Error};

/// import custom modules(network)
mod net;
pub use net::{TcpListener,Resolver,ResolveData};

/// import custom modules(utility)
mod utility;
mod cipher;

pub use cipher::*;
pub use utility::*;



/// declare the result<T,error> alias
///
/// # Examples
///
/// ```edition2018
/// fn main()->shadowsocks::Res<()>{
///     println!("hello.world!");
///     Ok(())
/// }
/// ```
pub type Res<T> = Result<T,Error>;






