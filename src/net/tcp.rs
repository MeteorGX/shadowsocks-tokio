mod inner {
    pub use std::sync::atomic::{AtomicU64,Ordering::Acquire,Ordering::Relaxed};
    pub use tokio::net::{TcpListener,TcpStream};
    pub use std::net::SocketAddr;
    pub use tokio::spawn;
    pub use tokio::time::{Duration,sleep};
    pub use std::future::Future;
}


/// Accepts a new incoming connection from this listener.
///
/// # Examples
/// ```no_run
/// use shadowsocks::{Res, TcpListener };
/// use tokio::net::TcpStream;
/// use std::net::Ipv4Addr;
///
/// #[tokio::test]
/// async fn tcp_server()->Res<()>{
///     let address = "127.0.0.1:7878";
///     let server = TcpListener::bind(address).await?;
///
///     let wakeup = async {
///         tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
///         let _ =TcpStream::connect(address).await;
///         Res::Ok(())
///     };
///
///     let executor = async move {
///         let (stream,_) = server.accept().await?;
///         assert_eq!(stream.peer_addr().unwrap().ip(),Ipv4Addr::new(127,0,0,1));
///         Res::Ok(())
///     };
///
///     let res = tokio::try_join!(wakeup,executor);
///     assert!(res.is_ok());
///     Ok(())
///  }
/// ```
pub struct TcpListener{
    socket: inner::TcpListener,
    backoff: inner::AtomicU64,
}

impl TcpListener{

    pub async fn bind(address:&str)->crate::Res<Self>{
        Ok(Self{
            socket: inner::TcpListener::bind(address).await?,
            backoff: inner::AtomicU64::new(0),
        })
    }

    pub async fn accept(&self)->crate::Res<(inner::TcpStream,inner::SocketAddr)>{
        return match self.socket.accept().await {
            Ok(res) => {
                self.backoff.store(0, inner::Relaxed);
                Ok(res)
            },
            Err(e) => {
                if self.backoff.load(inner::Acquire) > 64 {
                    std::process::exit(e.raw_os_error().unwrap());
                }

                inner::sleep(inner::Duration::from_secs(
                    self.backoff.load(inner::Acquire)
                )).await;

                self.backoff.store(self.backoff.load(inner::Acquire) * 2, inner::Relaxed);
                Err(e.into())
            }
        }
    }
}