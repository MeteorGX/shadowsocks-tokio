

#[allow(dead_code)]
mod resolve;

#[allow(dead_code)]
mod tcp;

pub use resolve::{Resolver,ResolveData};
pub use tcp::{TcpListener};