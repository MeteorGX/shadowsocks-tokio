use std::collections::HashMap;
use std::net::{IpAddr, ToSocketAddrs};

/// deconstructing the data returned by parsing
///
#[derive(Debug, PartialOrd, PartialEq, Copy, Clone)]
pub struct ResolveData{
    pub address:IpAddr,
    pub port:u16,
}


#[derive(Debug)]
pub struct Resolver{
   cache:HashMap<String,ResolveData>
}


impl Resolver{

    /// Creates a new Resolver Handler.
    ///
    /// If you have an idea of how much data the `HashMap` will hold,
    /// consider the [`with_capacity`] method to prevent excessive re-allocation cache.
    ///
    /// # Examples
    /// ```edition2018
    /// use shadowsocks::Resolver;
    /// let mut resolver = Resolver::new();
    /// ```
    ///
    pub fn new()->Self{
        Self{cache:HashMap::new()}
    }

    /// Creates a `Resolver` with a particular capacity.
    ///
    /// # Examples
    /// ```edition2018
    /// use shadowsocks::Resolver;
    /// let mut resolver = Resolver::with_capacity(1024);
    /// ```
    ///
    pub fn with_capacity(c:usize)->Self{
        Self{cache:HashMap::with_capacity(c)}
    }


    /// resolve the domain name and return the IP address
    ///
    /// accepts strings like 'www.google.com:80'
    ///
    /// # Examples
    /// ```edition2018
    /// use shadowsocks::Resolver;
    /// let mut resolver = Resolver::with_capacity(1024);
    /// let data = resolver.parse("www.google.com:80").unwrap();
    /// println!("IP = {:?},Port = {}",data.address,data.port);
    /// ```
    ///
    pub fn parse(&mut self,hostname:&str)->crate::Res<ResolveData>{
        if self.cache.contains_key(hostname){
            let exists = self.cache.get(hostname).unwrap();
            return Ok(exists.clone())
        }

        let res = match hostname[..].to_socket_addrs() {
            Ok(res) => res,
            Err(e) =>{
                return Err(e.into())
            }
        };
        crate::debug!("{:?}",res);

        if let Some(res) = res.into_iter().next(){
            let data = ResolveData{
                address:res.ip(),
                port:res.port(),
            };
            self.cache.insert(String::from(hostname),data.clone());
            Ok(data)
        }else{
            Err(crate::Error::ResolveParse)
        }
    }


    /// asynchronous resolve the domain name and return the IP address
    ///
    /// accepts strings like 'www.google.com:80'
    ///
    /// # Examples
    /// ```edition2018
    /// use shadowsocks::{Resolver,Res};
    ///
    /// #[tokio::main]
    /// async fn main()->Res<()>{
    ///     let mut resolver = Resolver::new();
    ///     let data = resolver.async_parse("www.google.com:80").await?;
    ///     println!("IP = {:?},Port = {}",data.address,data.port);
    ///     Ok(())
    /// }
    /// ```
    ///
    pub async fn async_parse(&mut self,hostname:&str)->crate::Res<ResolveData>{
        self.parse(hostname)
    }

}
