use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};
use crate::{CipherHandler};
use tokio::macros::support::{Future, Pin, Poll};
use std::task::Context;
use std::ops::DerefMut;
use std::sync::Arc;


#[derive(Debug)]
pub struct EncryptStream<'life,R:?Sized,W:?Sized>{
    cipher:Arc<CipherHandler>,
    reader:&'life mut R,
    writer:&'life mut W,
    done:bool,
    pos:usize,
    buffer:Vec<u8>,
    iv:Option<Vec<u8>>
}

#[derive(Debug)]
pub struct DecryptStream<'life,R:?Sized,W:?Sized>{
    cipher:Arc<CipherHandler>,
    reader:&'life mut R,
    writer:&'life mut W,
    done:bool,
    pos:usize,
    buffer:Vec<u8>,
    iv:Option<Vec<u8>>
}


impl<'life,R,W> EncryptStream<'life,R,W>
where R:AsyncRead + Unpin + ?Sized,W:AsyncWrite + Unpin + ?Sized{
    pub fn with_capacity(
        cipher:Arc<CipherHandler>,
        reader:&'life mut R,
        writer:&'life mut W,
        cap:usize
    )->Self{
        Self{
            cipher,
            reader,
            writer,
            done: false,
            pos: 0,
            buffer: vec![0u8; cap],
            iv:None
        }
    }
}


impl<'life,R,W> DecryptStream<'life,R,W>
    where R:AsyncRead + Unpin + ?Sized,W:AsyncWrite + Unpin + ?Sized{
    pub fn with_capacity(
        cipher:Arc<CipherHandler>,
        reader:&'life mut R,
        writer:&'life mut W,
        cap:usize
    )->Self{
        Self{
            cipher,
            reader,
            writer,
            done: false,
            pos: 0,
            buffer: vec![0u8; cap],
            iv: None
        }
    }
}




impl<R,W> Future for EncryptStream<'_,R,W>
    where R:AsyncRead + Unpin +?Sized, W:AsyncWrite + Unpin + ?Sized{
    type Output = tokio::io::Result<()>;
    fn poll(self:Pin<&mut Self>,cx:&mut Context<'_>)->Poll<Self::Output>{
        let handler = self.get_mut();

        // write cipher
        let iv_data = match handler.iv.as_ref(){
            None => {
                handler.iv = Some(handler.cipher.generate_iv());
                let iv = handler.iv.as_ref().unwrap();
                match Pin::new(handler.writer.deref_mut()).poll_write(cx,iv.as_slice()){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?;
                crate::debug!("IV Data = {:?}",handler.iv);
                iv
            }
            Some(res) => res
        };



        // encrypt data
        loop {
            // reset buffer
            for i in 0..handler.buffer.capacity() {
                handler.buffer[i] = 0u8;
            }

            // read
            let mut buffer = ReadBuf::new(handler.buffer.as_mut());
            match Pin::new(handler.reader.deref_mut()).poll_read(cx,&mut buffer){
                Poll::Ready(res) => res,
                Poll::Pending => { return Poll::Pending; }
            }?;
            let readable = buffer.filled().len();
            if readable == 0 { handler.done = true; }


            // write
            handler.pos = 0;
            while handler.pos < readable {
                let data = &buffer.filled()[handler.pos..readable];
                crate::debug!("Src[{}] = {:?}",data.len(),data);

                // encrypt text
                let encrypt_text = match handler.cipher.encrypt_with_iv(data,iv_data.as_slice()){
                    Ok(res) => res,
                    Err(e) => { return Poll::Ready(Err(e.into())) }
                };

                crate::debug!("Enc[{}] = {:?}",encrypt_text.len(),encrypt_text);

                let pos = match Pin::new(handler.writer.deref_mut()).poll_write(cx,encrypt_text.as_slice()){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?;
                if pos == 0 {
                    return Poll::Ready(Err(tokio::io::Error::from(tokio::io::ErrorKind::WriteZero)));
                } else {
                    handler.pos += pos;
                }
            };

            // flush
            if handler.done {
                return Poll::Ready(Ok(match Pin::new(handler.writer.deref_mut()).poll_flush(cx){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?));
            }
        }
    }
}


impl<R,W> Future for DecryptStream<'_,R,W>
    where R:AsyncRead + Unpin +?Sized, W:AsyncWrite + Unpin + ?Sized{
    type Output = tokio::io::Result<()>;
    fn poll(self:Pin<&mut Self>,cx:&mut Context<'_>)->Poll<Self::Output>{
        let handler = self.get_mut();

        // read cipher
        let iv_data = match handler.iv.as_ref() {
            None => {
                let mut iv_buffer = ReadBuf::new(&mut handler.buffer[0..handler.cipher.get_iv_len()]);
                match Pin::new(handler.reader.deref_mut()).poll_read(cx,&mut iv_buffer){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?;
                handler.iv = Some(iv_buffer.filled().to_vec());
                crate::debug!("IV Data = {:?}",handler.iv);
                handler.iv.as_ref().unwrap()
            },
            Some(res) => res
        };


        // decrypt data
        loop {
            // reset buffer
            for i in 0..handler.buffer.capacity() {
                handler.buffer[i] = 0u8;
            }

            // read
            let mut buffer = ReadBuf::new(handler.buffer.as_mut());
            match Pin::new(handler.reader.deref_mut()).poll_read(cx,&mut buffer){
                Poll::Ready(res) => res,
                Poll::Pending => { return Poll::Pending; }
            }?;
            let readable = buffer.filled().len();
            if readable == 0 { handler.done = true; }


            // write
            handler.pos = 0;
            while handler.pos < readable {
                let data = &buffer.filled()[handler.pos..readable];
                crate::debug!("Src[{}] = {:?}",data.len(),data);

                // decrypt text
                let decrypt_text = match handler.cipher.decrypt_with_iv(data,iv_data.as_slice()){
                    Ok(res) => res,
                    Err(e) => { return Poll::Ready(Err(e.into())) }
                };
                crate::debug!("Dec[{}] = {:?}",decrypt_text.len(),decrypt_text);

                let pos = match Pin::new(handler.writer.deref_mut()).poll_write(cx,decrypt_text.as_slice()){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?;
                if pos == 0 {
                    return Poll::Ready(Err(tokio::io::Error::from(tokio::io::ErrorKind::WriteZero)));
                } else {
                    handler.pos += pos;
                }
            };

            // flush
            if handler.done {
                return Poll::Ready(Ok(match Pin::new(handler.writer.deref_mut()).poll_flush(cx){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?));
            }
        }
    }
}
