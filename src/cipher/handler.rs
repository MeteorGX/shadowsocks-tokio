use tokio::io::{AsyncWrite, AsyncWriteExt};

mod ssl{
    pub use openssl::symm::{Cipher,encrypt,decrypt};
    use rand::Rng;

    ///
    ///
    ///
    #[inline]
    pub fn match_cipher(method:&str)->Option<Cipher>{
        return match method {

            // aes 128 bit
            "aes_128_ecb" => Some(Cipher::aes_128_ecb()),
            "aes_128_cbc" => Some(Cipher::aes_128_cbc()),
            "aes_128_xts" => Some(Cipher::aes_128_xts()),
            "aes_128_ctr" => Some(Cipher::aes_128_ctr()),
            "aes_128_cfb1" => Some(Cipher::aes_128_cfb1()),
            "aes_128_cfb128" => Some(Cipher::aes_128_cfb128()),
            "aes_128_cfb8" => Some(Cipher::aes_128_cfb8()),
            "aes_128_gcm" => Some(Cipher::aes_128_gcm()),
            "aes_128_ccm" => Some(Cipher::aes_128_ccm()),
            "aes_128_ofb" => Some(Cipher::aes_128_ofb()),
            "aes_128_ocb" => Some(Cipher::aes_128_ocb()),

            // aes 192 bit
            "aes_192_ecb" => Some(Cipher::aes_192_ecb()),
            "aes_192_cbc" => Some(Cipher::aes_192_cbc()),
            "aes_192_ctr" => Some(Cipher::aes_192_ctr()),
            "aes_192_cfb1" => Some(Cipher::aes_192_cfb1()),
            "aes_192_cfb128" => Some(Cipher::aes_192_cfb128()),
            "aes_192_cfb8" => Some(Cipher::aes_192_cfb8()),
            "aes_192_gcm" => Some(Cipher::aes_192_gcm()),
            "aes_192_ccm" => Some(Cipher::aes_192_ccm()),
            "aes_192_ofb" => Some(Cipher::aes_192_ofb()),
            "aes_192_ocb" => Some(Cipher::aes_192_ocb()),

            // aes 256 bit
            "aes_256_ecb" => Some(Cipher::aes_256_ecb()),
            "aes_256_cbc" => Some(Cipher::aes_256_cbc()),
            "aes_256_xts" => Some(Cipher::aes_256_xts()),
            "aes_256_ctr" => Some(Cipher::aes_256_ctr()),
            "aes_256_cfb1" => Some(Cipher::aes_256_cfb1()),
            "aes_256_cfb128" => Some(Cipher::aes_256_cfb128()),
            "aes_256_cfb8" => Some(Cipher::aes_256_cfb8()),
            "aes_256_gcm" => Some(Cipher::aes_256_gcm()),
            "aes_256_ccm" => Some(Cipher::aes_256_ccm()),
            "aes_256_ofb" => Some(Cipher::aes_256_ofb()),
            "aes_256_ocb" => Some(Cipher::aes_256_ocb()),


            // other
            "des_cbc" => Some(Cipher::des_cbc()),
            "des_ecb" => Some(Cipher::des_ecb()),
            "des_ede3" => Some(Cipher::des_ede3()),
            "des_ede3_cbc" => Some(Cipher::des_ede3_cbc()),
            "des_ede3_cfb64" => Some(Cipher::des_ede3_cfb64()),
            "rc4" => Some(Cipher::rc4()),
            "chacha20" => Some(Cipher::chacha20()),
            "chacha20_poly1305" => Some(Cipher::chacha20_poly1305()),
            _ => None,
        };
    }


    ///
    ///
    ///
    #[inline]
    pub fn generate_key(password:&[u8],sz:usize)->Vec<u8>{
        static CIPHER_KEY_LENGTH:u32 = 16;
        let sz = (sz as f32 / CIPHER_KEY_LENGTH as f32).ceil() as u32;
        let mut key = Vec::from(&md5::compute(password)[..]);

        let mut offset = 0;
        for _ in 1..sz {
            offset += CIPHER_KEY_LENGTH;
            let begin = (offset - CIPHER_KEY_LENGTH) as usize;

            let mut d = Vec::from(&key[begin..offset as usize]);
            d.extend_from_slice(password);
            let d = md5::compute(d);
            key.extend_from_slice(&*d);
        }
        key
    }


    ///
    ///
    ///
    #[inline]
    pub fn generate_iv(sz:usize)->Vec<u8>{
        rand::thread_rng().sample_iter(
            &rand::distributions::Standard
        ).take(sz).collect()
    }
}

///
///
///
#[derive(Clone)]
pub struct CipherHandler{
    cipher:ssl::Cipher,
    key:Vec<u8>,
    method:String,
}


impl CipherHandler{


    ///
    ///
    ///
    pub fn new(method:&str,password:&str)->crate::Res<Self>{
        let cipher = match ssl::match_cipher(method) {
            Some(res) => res,
            None =>{  return Err(crate::Error::CipherMethodUnSupport) }
        };
        let key = ssl::generate_key(password.as_bytes(),cipher.key_len());
        Ok(Self{ cipher, key, method:method.to_string() })
    }


    pub fn generate_iv(&self)->Vec<u8>{
        ssl::generate_iv(self.get_iv_len())
    }

    ///
    ///
    ///
    pub fn encrypt_with_iv(&self,ctx:&[u8],iv:&[u8])->crate::Res<Vec<u8>>{
        return match ssl::encrypt(
            self.cipher,
            self.key.as_slice(),
            Some(iv.as_ref()),
            ctx
        ){
            Ok(res) => { Ok(res) },
            Err(e) => { Err(e.into()) }
        };
    }

    pub async fn async_encrypt_with_iv(&self,ctx:&[u8],iv:&[u8])->crate::Res<Vec<u8>>{
        self.encrypt_with_iv(ctx,iv)
    }

    pub fn decrypt_with_iv(&self,ctx:&[u8],iv:&[u8])->crate::Res<Vec<u8>>{
        match ssl::decrypt(
            self.cipher,
            self.key.as_slice(),
            Some(iv.as_ref()),
            ctx
        ){
            Ok(res) => Ok(res),
            Err(e) => { return Err(e.into()); }
        }
    }

    pub async fn async_decrypt_with_iv(&self,ctx:&[u8],iv:&[u8])->crate::Res<Vec<u8>>{
        self.decrypt_with_iv(ctx,iv)
    }
    pub fn get_iv_len(&self)->usize{
        self.cipher.iv_len().unwrap_or_default()
    }

    pub fn get_key_len(&self)->usize{
        self.cipher.key_len()
    }



    pub async fn write_bytes<W>(&self,writer:&mut W,byte:&[u8])->crate::Res<()>
        where W:AsyncWrite + Unpin + ?Sized{
        let mut iv = self.generate_iv();
        let encrypt_text = self.encrypt_with_iv(byte,iv.as_slice())?;
        iv.extend_from_slice(encrypt_text.as_slice());
        Ok(writer.write_all(iv.as_slice()).await?)
    }


}


impl std::fmt::Debug for CipherHandler{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("")
            .field(&self.key)
            .field(&self.method)
            .finish()
    }
}
