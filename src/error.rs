
mod f {
    pub use std::fmt::{Display,Formatter,Result};
}

/// generic error box types.
///
/// declare the Box<Error> container to place the error object.
///
pub type EBox = Box<dyn std::error::Error+Sync+Send>;



/// generic custom error types
///
///
#[derive(Debug)]
pub enum Error{
    ResolveParse,
    CipherMethodUnSupport,
    SocksParseFailed,
    Other(EBox)
}


/// extend custom error( String )
///
/// # Examples
///
/// ```edition2018
/// use shadowsocks::Error;
/// use std::any::{Any, TypeId};
///
/// fn main(){
///     let string_error:Error = String::from("hello. my error!").into();
///     assert_eq!(string_error.type_id(),TypeId::of::<Error>()); // shadowsocks::Error == String::into()
/// }
/// ```
impl From<String> for Error{
    fn from(src:String)->Self{ Error::Other(src.into()) }
}


/// extend custom error( &str )
///
/// # Examples
///
/// ```edition2018
/// use shadowsocks::Error;
/// use std::any::{Any, TypeId};
///
/// fn main(){
///     let str_error:Error = "hello. my error!".into();
///     assert_eq!(str_error.type_id(),TypeId::of::<Error>()); // shadowsocks::Error == &str::into()
/// }
/// ```
impl From<&str> for Error{
    fn from(src:&str)->Self{ src.to_string().into() }
}


/// extend custom error( &str )
///
impl From<EBox> for Error{
    fn from(src: EBox) -> Self {
        Error::Other(src)
    }
}

/// extend log error
///
/// # Examples
///
/// ```edition2018
/// fn main()->Result<(),shadowsocks::EBox>{
///     env_logger::try_init()?;
///     println!("use custom EBox by env_logger");
///     Ok(())
/// }
/// ```
impl From<log::SetLoggerError> for Error{
    fn from(src: log::SetLoggerError) -> Self {
        src.to_string().into()
    }
}


/// extend tokio IO error
///
/// # Examples
///
/// ```edition2018
///
/// #[tokio::main]
/// async fn main()->Result<(),shadowsocks::EBox>{
///     println!("use custom EBox by tokio");
///     Ok(())
/// }
/// ```
impl From<tokio::io::Error> for Error{
    fn from(src: tokio::io::Error) -> Self{ src.to_string().into() }
}


impl From<tokio::task::JoinError> for Error{
    fn from(src: tokio::task::JoinError)->Self{ src.to_string().into() }
}


impl From<openssl::error::ErrorStack> for Error{
    fn from(src: openssl::error::ErrorStack)->Self{ src.to_string().into() }
}

impl From<std::str::Utf8Error> for Error{
    fn from(src: std::str::Utf8Error) -> Self {
        src.to_string().into()
    }
}

impl From<std::net::AddrParseError> for Error{
    fn from(src: std::net::AddrParseError) -> Self {
        src.to_string().into()
    }
}

impl From<tokio::time::error::Elapsed> for Error{
    fn from(src: tokio::time::error::Elapsed) -> Self {
        src.to_string().into()
    }
}

impl From<std::num::ParseIntError> for Error{
    fn from(src: std::num::ParseIntError) -> Self {
        src.to_string().into()
    }
}



impl From<Error> for tokio::io::Error{
    fn from(src: Error) -> Self {
        src.into()
    }
}


#[allow(unreachable_patterns)]
impl f::Display for Error{
    fn fmt(&self, f: &mut f::Formatter<'_>) -> f::Result {
        match self{
            Error::ResolveParse => "failed by resolve hostname".fmt(f),
            Error::CipherMethodUnSupport => "cipher method unsupport".fmt(f),
            Error::SocksParseFailed => "failed by socks parse".fmt(f),
            Error::Other(e) => e.fmt(f),
            _ => "unknown error".fmt(f),// todo: test need
        }
    }
}


/// extended inheritance system error
///
impl std::error::Error for Error{ }
