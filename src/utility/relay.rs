use tokio::io::{AsyncWrite, AsyncRead, ReadBuf};
use tokio::macros::support::Future;
use std::task::{Context, Poll};
use std::pin::Pin;
use std::ops::{DerefMut};

#[derive(Debug)]
pub struct Relay<'life,R:?Sized,W:?Sized>{
    reader:&'life mut R,
    writer:&'life mut W,
    buffer:Vec<u8>,
    pos:usize,
    done:bool,
}


impl<'life,R,W> Relay<'life,R,W>
    where R:AsyncRead + Unpin +?Sized, W:AsyncWrite + Unpin + ?Sized{

    pub fn with_capacity(reader:&'life mut R,writer:&'life mut W,capacity:usize)->Self{
        Self{ reader,writer,buffer:vec![0u8;capacity], pos: 0, done: false }
    }
}


impl<R,W> Future for Relay<'_,R,W>
    where R:AsyncRead + Unpin +?Sized, W:AsyncWrite + Unpin + ?Sized{
    type Output = tokio::io::Result<()>;
    fn poll(self:Pin<&mut Self>,cx:&mut Context<'_>)->Poll<Self::Output>{
        let handler = self.get_mut();

        loop {
            // reset buffer
            for i in 0..handler.buffer.capacity() {
                handler.buffer[i] = 0u8;
            }

            // read
            let mut buffer = ReadBuf::new(handler.buffer.as_mut());
            match Pin::new(handler.reader.deref_mut()).poll_read(cx,&mut buffer){
                Poll::Ready(res) => res,
                Poll::Pending => { return Poll::Pending; }
            }?;
            let readable = buffer.filled().len();
            if readable == 0 { handler.done = true; }


             // write
            handler.pos = 0;
            while handler.pos < readable {
                let data = &buffer.filled()[handler.pos..readable];
                let pos = match Pin::new(handler.writer.deref_mut()).poll_write(cx,data){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?;
                if pos == 0 {
                    return Poll::Ready(Err(tokio::io::Error::from(tokio::io::ErrorKind::WriteZero)));
                } else {
                    handler.pos += pos;
                }
            };

            // flush
            if handler.done {
                return Poll::Ready(Ok(match Pin::new(handler.writer.deref_mut()).poll_flush(cx){
                    Poll::Ready(res) => res,
                    Poll::Pending => { return Poll::Pending; }
                }?));
            }
        }
    }
}
