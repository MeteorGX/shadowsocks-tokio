use bytes::{BufMut, BytesMut};
use tokio::io::{AsyncWrite, AsyncWriteExt, AsyncRead, AsyncReadExt};
use std::net::{Ipv4Addr, Ipv6Addr};


#[allow(dead_code)]
pub mod inner{
    pub const UNKNOWN:u8 = 0xff;
    pub const SOCKS_RSV:u8 = 0x00;
    pub const SOCKS5_VERSION:u8 = 0x05;



    /// Field: Methods
    pub const SOCKS_METHOD_NO_AUTH:u8 = 0x00;
    pub const SOCKS_METHOD_GSSAPI:u8 = 0x01;
    pub const SOCKS_METHOD_NORMAL:u8 = 0x02;

    /// Field: CMD
    pub const SOCKS_CMD_CONNECT:u8 = 0x01;
    pub const SOCKS_CMD_BIND:u8 = 0x02;
    pub const SOCKS_CMD_UDP:u8 = 0x03;

    /// Field: Address
    pub const SOCKS_ADDR_IPV4:u8 = 0x01;
    pub const SOCKS_ADDR_IPV6:u8 = 0x04;
    pub const SOCKS_ADDR_HOSTNAME:u8 = 0x03;

    /// Field: Rep
    pub const SOCKS_REP_SUCCESS:u8 = 0x00;
    pub const SOCKS_REP_FAILED:u8 = 0x01;
    pub const SOCKS_REP_NOT_ALLOW:u8 = 0x02;
    pub const SOCKS_REP_NET_UNREACHABLE:u8 = 0x03;
    pub const SOCKS_REP_HOST_UNREACHABLE:u8 = 0x04;
    pub const SOCKS_REP_CONNECT_REFUSED:u8 = 0x05;
    pub const SOCKS_REP_TTL_EXPIRED:u8 = 0x06;
    pub const SOCKS_REP_CMD_NOT_SUPPORT:u8 = 0x07;
    pub const SOCKS_REP_ADDRESS_NOT_SUPPORT:u8 = 0x08;


    pub const SOCKS_ADDR_LEN_IPV4:usize = 6;
    pub const SOCKS_ADDR_LEN_IPV6:usize = 18;
}




///
///
///
#[derive(Debug,Clone,Copy,PartialEq)]
pub enum SocksVersion {
    V5,
    Unknown
}


impl From<SocksVersion> for u8{
    fn from(src: SocksVersion) -> Self {
        match src {
            SocksVersion::V5 => { inner::SOCKS5_VERSION }
            _ => { inner::UNKNOWN }
        }
    }
}

impl From<u8> for SocksVersion{
    fn from(src: u8) -> Self {
        match src {
            inner::SOCKS5_VERSION => { Self::V5 }
            _ => { Self::Unknown }
        }
    }
}


///
///
///
#[derive(Debug,Clone,Copy)]
pub enum SocksMethod{
    NoAuth,
    GSSAPI,
    Normal,
    Unknown
}

impl From<SocksMethod> for u8{
    fn from(src: SocksMethod) -> Self {
        match src {
            SocksMethod::NoAuth => inner::SOCKS_METHOD_NO_AUTH,
            SocksMethod::GSSAPI => inner::SOCKS_METHOD_GSSAPI,
            SocksMethod::Normal => inner::SOCKS_METHOD_NORMAL,
            SocksMethod::Unknown => inner::UNKNOWN
        }
    }
}

impl From<u8> for SocksMethod{
    fn from(src: u8) -> Self {
        match src {
            inner::SOCKS_METHOD_NO_AUTH => { Self::NoAuth }
            inner::SOCKS_METHOD_GSSAPI => { Self::GSSAPI }
            inner::SOCKS_METHOD_NORMAL => { Self::Normal }
            _ => { Self::Unknown }
        }
    }
}


///
///
///
#[derive(Debug,Clone,Copy)]
pub enum SocksCommand {
    Connect,
    Bind,
    UDP,
    Unknown
}


impl From<SocksCommand> for u8{
    fn from(src: SocksCommand) -> Self {
        match src {
            SocksCommand::Connect => inner::SOCKS_CMD_CONNECT,
            SocksCommand::Bind => inner::SOCKS_CMD_BIND,
            SocksCommand::UDP => inner::SOCKS_CMD_UDP,
            SocksCommand::Unknown => inner::UNKNOWN
        }
    }
}

impl From<u8> for SocksCommand{
    fn from(src: u8) -> Self {
        match src {
            inner::SOCKS_CMD_CONNECT => { Self::Connect }
            inner::SOCKS_CMD_BIND => { Self::Bind }
            inner::SOCKS_CMD_UDP => { Self::UDP }
            _ => { Self::Unknown }
        }
    }
}




///
///
///
#[derive(Debug,Clone)]
pub enum SocksAddress {
    IPv4(Ipv4Addr,u16),
    IPv6(Ipv6Addr,u16),
    Hostname(String,u16),
    Unknown
}

impl From<SocksAddress> for u8{
    fn from(src: SocksAddress) -> Self {
        match src {
            SocksAddress::IPv4(_,_) => inner::SOCKS_ADDR_IPV4,
            SocksAddress::IPv6(_,_) => inner::SOCKS_ADDR_IPV6,
            SocksAddress::Hostname(_,_) => inner::SOCKS_ADDR_HOSTNAME,
            _ => { inner::UNKNOWN }
        }
    }
}

impl From<&SocksAddress> for u8{
    fn from(src: &SocksAddress) -> Self {
        src.to_owned().into()
    }
}


///
///
///
#[derive(Debug,Clone)]
pub enum SocksResponse {
    Success,
    Failed,
    NotAllow,
    NetUnreachable,
    HostUnreachable,
    ConnectionRefused,
    TTLExpired,
    CommandNotSupport,
    AddressTypeNotSupport,
    Unknown
}


impl From<SocksResponse> for u8{
    fn from(src: SocksResponse) -> Self {
        match src {
            SocksResponse::Success => inner::SOCKS_REP_SUCCESS,
            SocksResponse::Failed => inner::SOCKS_REP_FAILED,
            SocksResponse::NotAllow => inner::SOCKS_REP_NOT_ALLOW,
            SocksResponse::NetUnreachable =>inner::SOCKS_REP_NET_UNREACHABLE,
            SocksResponse::HostUnreachable => inner::SOCKS_REP_HOST_UNREACHABLE,
            SocksResponse::ConnectionRefused => inner::SOCKS_REP_CONNECT_REFUSED,
            SocksResponse::TTLExpired => inner::SOCKS_REP_TTL_EXPIRED,
            SocksResponse::CommandNotSupport => inner::SOCKS_REP_CMD_NOT_SUPPORT,
            SocksResponse::AddressTypeNotSupport => inner::SOCKS_REP_ADDRESS_NOT_SUPPORT,
            _ => inner::UNKNOWN
        }
    }
}

impl From<&SocksResponse> for u8{
    fn from(src: &SocksResponse) -> Self {
        src.to_owned().into()
    }
}

impl From<u8> for SocksResponse{
    fn from(src: u8) -> Self {
        match src {
            inner::SOCKS_REP_SUCCESS => { Self::Success }
            inner::SOCKS_REP_FAILED => { Self::Failed }
            inner::SOCKS_REP_NOT_ALLOW => { Self::NotAllow }
            inner::SOCKS_REP_NET_UNREACHABLE => { Self::NetUnreachable }
            inner::SOCKS_REP_HOST_UNREACHABLE => { Self::HostUnreachable }
            inner::SOCKS_REP_CONNECT_REFUSED => { Self::ConnectionRefused }
            inner::SOCKS_REP_TTL_EXPIRED => { Self::TTLExpired }
            inner::SOCKS_REP_CMD_NOT_SUPPORT => { Self::CommandNotSupport }
            inner::SOCKS_REP_ADDRESS_NOT_SUPPORT => { Self::AddressTypeNotSupport }
            _ => { Self::Unknown }
        }
    }
}


///
///
///
#[derive(Debug,Clone)]
pub struct SocksBuilder{
    version:SocksVersion,
    method:SocksMethod,
    buffer:BytesMut
}

#[derive(Debug,Clone)]
pub struct SocksData{
    pub cmd:SocksCommand,
    pub address:SocksAddress
}


impl SocksBuilder{


    ///
    ///
    ///
    pub fn witch_method(version:SocksVersion,method:SocksMethod)->Self{
        Self{version,method,buffer: BytesMut::new()}
    }


    ///
    ///
    ///
    pub async fn match_version<R>(&self,reader:&mut R)->crate::Res<()>
        where R:AsyncRead + Unpin + ?Sized{
        if reader.read_u8().await? != self.version.into() {
            Err(crate::Error::SocksParseFailed)
        }else{
            Ok(())
        }
    }


    ///
    ///
    ///
    fn fill_buffer(&mut self,sz:usize){
        self.buffer.clear();
        for _ in 0..sz {
            self.buffer.put_u8(0u8)
        }
    }

    ///
    ///
    ///
    pub async fn match_cmd<R>(&self,reader:&mut R)->crate::Res<SocksCommand>
        where R:AsyncRead + Unpin + ?Sized{
        Ok(reader.read_u8().await?.into())
    }

    ///
    ///
    ///
    pub async fn match_rsv<R>(&self,reader:&mut R)->crate::Res<()>
        where R:AsyncRead + Unpin + ?Sized{
        if reader.read_u8().await? == inner::SOCKS_RSV {
            Ok(())
        }else {
            Err(crate::Error::SocksParseFailed)
        }
    }


    ///
    ///
    ///
    pub async fn match_address<R>(&mut self,reader:&mut R)->crate::Res<SocksAddress>
        where R:AsyncRead + Unpin + ?Sized{
        match reader.read_u8().await?{

            inner::SOCKS_ADDR_IPV4 =>{
                self.fill_buffer(inner::SOCKS_ADDR_LEN_IPV4 + 2);
                reader.read_exact(self.buffer.as_mut()).await?;
                let address = Ipv4Addr::new(
                    self.buffer[0],
                    self.buffer[1],
                    self.buffer[2],
                    self.buffer[3]);
                let port = ((self.buffer[4] as u16) << 8) | (self.buffer[5] as u16);
                Ok(SocksAddress::IPv4(address,port))
            },

            inner::SOCKS_ADDR_IPV6 =>{
                self.fill_buffer(inner::SOCKS_ADDR_LEN_IPV6 + 2);
                reader.read_exact(self.buffer.as_mut()).await?;
                let split = self.buffer.len() - 2;

                let a = ((self.buffer[0] as u16) << 8) | (self.buffer[1] as u16);
                let b = ((self.buffer[2] as u16) << 8) | (self.buffer[3] as u16);
                let c = ((self.buffer[4] as u16) << 8) | (self.buffer[5] as u16);
                let d = ((self.buffer[6] as u16) << 8) | (self.buffer[7] as u16);
                let e = ((self.buffer[8] as u16) << 8) | (self.buffer[9] as u16);
                let f = ((self.buffer[10] as u16) << 8) | (self.buffer[11] as u16);
                let g = ((self.buffer[12] as u16) << 8) | (self.buffer[13] as u16);
                let h = ((self.buffer[14] as u16) << 8) | (self.buffer[15] as u16);

                let address = Ipv6Addr::new(a, b, c, d, e, f, g, h);
                let port = ((self.buffer[split] as u16) << 8) | (self.buffer[split + 1] as u16);
                Ok(SocksAddress::IPv6(address,port))
            },
            inner::SOCKS_ADDR_HOSTNAME =>{
                let length = reader.read_u8().await? as usize;
                self.fill_buffer(length + 2);

                reader.read_exact(self.buffer.as_mut()).await?;
                let split = self.buffer.len() - 2;

                let address = &self.buffer[..split];
                let address = match std::str::from_utf8(address){
                    Ok(res) => res.to_string(),
                    Err(e) => {
                        return Err(e.into());
                    }
                };
                let port = ((self.buffer[split] as u16) << 8) | (self.buffer[split + 1] as u16);
                Ok(SocksAddress::Hostname(address,port))
            },
            _ => Ok(SocksAddress::Unknown)
        }
    }


    ///
    ///
    ///
    pub async fn read_methods<R>(&self,reader:&mut R)->crate::Res<Vec<u8>>
        where R:AsyncRead + Unpin + ?Sized{
        self.match_version(reader).await?;// version
        let length = reader.read_u8().await? as usize;
        let mut methods = Vec::with_capacity(length);
        for _ in 0..length {
            methods.push(reader.read_u8().await?);
        }
        Ok(methods)
    }


    ///
    ///
    ///
    pub async fn write_methods<W>(&mut self,writer:&mut W)->crate::Res<()>
        where W:AsyncWrite + Unpin + ?Sized{
        self.buffer.clear();
        self.buffer.put_u8(self.version.into());
        self.buffer.put_u8(1);
        self.buffer.put_u8(self.method.into());
        Ok(writer.write_all(self.buffer.as_ref()).await?)
    }



    ///
    ///
    ///
    pub async fn write_method<W>(&mut self,writer:&mut W)->crate::Res<()>
        where W:AsyncWrite + Unpin + ?Sized{
        self.buffer.clear();
        self.buffer.put_u8(self.version.into());
        self.buffer.put_u8(self.method.into());
        Ok(writer.write_all(self.buffer.as_ref()).await?)
    }


    ///
    ///
    ///
    pub async fn read_method<R>(&mut self,reader:&mut R)->crate::Res<SocksMethod>
        where R:AsyncRead + Unpin + ?Sized{
        self.match_version(reader).await?;// version
        Ok(reader.read_u8().await?.into())
    }



    ///
    ///
    ///
    pub async fn write_address<R>(&mut self,writer:&mut R,addr:&SocksData)->crate::Res<()>
        where R:AsyncWrite + Unpin + ?Sized{
        self.buffer.clear();
        self.buffer.put_u8(self.version.into());
        self.buffer.put_u8(addr.cmd.into());
        self.buffer.put_u8(inner::SOCKS_RSV);
        self.buffer.put_u8(addr.address.to_owned().into());
        match &addr.address {
            SocksAddress::IPv4(address,port) =>{
                self.buffer.put_slice(&address.octets());
                self.buffer.put_u16(*port);
            },
            SocksAddress::IPv6(address,port) =>{
                self.buffer.put_slice(&address.octets());
                self.buffer.put_u16(*port);
            },
            SocksAddress::Hostname(address,port) =>{
                self.buffer.put_u8(address.as_bytes().len() as u8);
                self.buffer.put_slice(address.as_bytes());
                self.buffer.put_u16(*port);
            }
            _ => { return Err(crate::Error::SocksParseFailed) }
        }

        Ok(writer.write_all(self.buffer.as_ref()).await?)
    }



    ///
    ///
    ///
    pub async fn read_address<R>(&mut self,reader:&mut R)->crate::Res<SocksData>
        where R:AsyncRead + Unpin + ?Sized{
        self.match_version(reader).await?;// version
        let cmd = self.match_cmd(reader).await?;//cmd
        self.match_rsv(reader).await?;//rsv
        let address = self.match_address(reader).await?;//address
        Ok(SocksData{
            cmd,
            address
        })
    }



    ///
    ///
    ///
    pub async fn write_response<W>(&mut self,writer:&mut W,rep:&SocksResponse,address:&SocksAddress)->crate::Res<()>
        where W:AsyncWrite + Unpin + ?Sized{
        self.buffer.clear();
        self.buffer.put_u8(self.version.into());
        self.buffer.put_u8(rep.into());
        self.buffer.put_u8(inner::SOCKS_RSV);

        let address_type = address.into();
        self.buffer.put_u8(address_type);
        match address {
            SocksAddress::IPv4(address,port) => {
                self.buffer.put_slice(&address.octets());
                self.buffer.put_u16(*port);
            },
            SocksAddress::IPv6(address,port)=> {
                self.buffer.put_slice(&address.octets());
                self.buffer.put_u16(*port);
            },
            SocksAddress::Hostname(address,port) =>{
                self.buffer.put_u8(address.as_bytes().len() as u8);
                self.buffer.put_slice(address.as_bytes());
                self.buffer.put_u16(*port);
            }
            _ => { return Err(crate::Error::SocksParseFailed); }
        };

        Ok(writer.write_all(self.buffer.as_ref()).await?)
    }


    ///
    ///
    ///
    pub async fn read_response<R>(&mut self,reader:&mut R)->crate::Res<(SocksResponse,SocksAddress)>
        where R:AsyncRead + Unpin + ?Sized{
        self.match_version(reader).await?;
        let rep = reader.read_u8().await?.into();
        self.match_rsv(reader).await?; // rsv
        let address = self.match_address(reader).await?;
        Ok((rep,address))
    }
}
