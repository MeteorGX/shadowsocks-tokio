use shadowsocks::{Res, TcpListener };
use tokio::net::TcpStream;
use std::net::Ipv4Addr;


#[tokio::test]
async fn tcp_server()->Res<()>{
    let address = "127.0.0.1:7878";
    let server = TcpListener::bind(address).await?;

    let wakeup = async {
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        let _ =TcpStream::connect(address).await;
        Res::Ok(())
    };

    let executor = async move {
        let (stream,_) = server.accept().await?;
        assert_eq!(stream.peer_addr().unwrap().ip(),Ipv4Addr::new(127,0,0,1));
        Res::Ok(())
    };

    let res = tokio::try_join!(wakeup,executor);
    assert!(res.is_ok());
    Ok(())
}