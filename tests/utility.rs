use shadowsocks::{Res, CipherHandler};

#[tokio::test]
async fn tcp_server()->Res<()>{

    let password = "MeteorCat";
    let message = "hello.world!";

    let cipher = CipherHandler::new("chacha20_poly1305",password)?;
    let iv = cipher.generate_iv();

    // encrypt data
    let encrypt_data = cipher.async_encrypt_with_iv(message.as_bytes(),iv.as_slice()).await?;

    // decrypt data
    let decrypt_data = cipher.async_encrypt_with_iv(encrypt_data.as_slice(),iv.as_slice()).await?;

    // compare
    assert_eq!(decrypt_data.as_slice(),message.as_bytes());

    Ok(())
}
