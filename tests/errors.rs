use shadowsocks::{Error,Res};
use std::any::{Any, TypeId};

#[test]
fn custom_errors()->Res<()>{
    let string_error:Error = String::from("hello. my error!").into();
    assert_eq!(string_error.type_id(),TypeId::of::<Error>()); // shadowsocks::Error == String::into()

    let str_error:Error = "hello. my error!".into();
    assert_eq!(str_error.type_id(),TypeId::of::<Error>()); // shadowsocks::Error == &str::into()

    Ok(())
}

