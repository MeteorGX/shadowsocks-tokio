use shadowsocks::{Res, Resolver};

#[test]
fn parse_hostname() ->Res<()>{
    let hostname = "www.bitbucket.org:80"; // format => hostname:port
    let mut resolver = Resolver::new();
    let res = resolver.parse(hostname)?;

    assert_eq!(res.port,80);
    Ok(())
}